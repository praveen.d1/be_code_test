-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2023 at 02:24 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `be_code_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2019_12_20_094217_create_organisations_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('1b6a18053943425ba7fbb5e47c15588487a3b95feb56723b3e038e4d0d3f5da971870bdf0d33efae', 11, 1, NULL, '[]', 0, '2023-10-31 03:23:21', '2023-10-31 03:23:21', '2024-10-31 08:53:21'),
('656da51882c7309a7936ad859be31de44a909f9644e28140780bc1f1348583922b32606e55417830', 11, 1, NULL, '[]', 0, '2023-11-01 04:32:17', '2023-11-01 04:32:17', '2024-11-01 10:02:17'),
('83765136a24f57ee99f2b26d0ba7dfa21e78c333754c6c84b4e13ea30d720b488bc917bde51b5795', 11, 1, NULL, '[]', 0, '2023-11-01 07:17:52', '2023-11-01 07:17:52', '2024-11-01 12:47:52');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Password Grant Client', 'BrbgsvYyFtcb9dC76aUMYCnYumKZBdjVVT4qyVWs', 'users', 'http://localhost', 0, 1, 0, '2023-10-31 03:17:54', '2023-10-31 03:17:54');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2023-10-31 03:16:54', '2023-10-31 03:16:54');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('14dfc81989043e3de23e9d1e5383a6ef2a261278f2c74587f165e5333bfbad61e2d4f0ffff8d1d59', '83765136a24f57ee99f2b26d0ba7dfa21e78c333754c6c84b4e13ea30d720b488bc917bde51b5795', 0, '2024-11-01 12:47:52'),
('88c6977ccc354dfbab482821af6249013dc88133986d5586fc5ff980c40c5f77d411d8d1c526f19c', '656da51882c7309a7936ad859be31de44a909f9644e28140780bc1f1348583922b32606e55417830', 0, '2024-11-01 10:02:17'),
('c8371e59b54c7a82670264d0f7ecdb82ec46ad16e5e2ecc48df4077b0b3b948dd860af362e0fb391', '1b6a18053943425ba7fbb5e47c15588487a3b95feb56723b3e038e4d0d3f5da971870bdf0d33efae', 0, '2024-10-31 08:53:21');

-- --------------------------------------------------------

--
-- Table structure for table `organisations`
--

CREATE TABLE `organisations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_user_id` int(11) NOT NULL,
  `trial_end` datetime DEFAULT NULL,
  `subscribed` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organisations`
--

INSERT INTO `organisations` (`id`, `name`, `owner_user_id`, `trial_end`, `subscribed`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Schmeler, Kris and Prosacco', 1, NULL, 1, '2023-10-31 03:16:19', '2023-10-31 03:16:19', NULL),
(2, 'Walsh-Aufderhar', 2, '2023-11-30 08:46:19', 0, '2023-10-31 03:16:19', '2023-10-31 03:16:19', NULL),
(3, 'Schimmel, Lehner and Turner', 3, '2023-11-30 08:46:19', 0, '2023-10-31 03:16:19', '2023-10-31 03:16:19', NULL),
(4, 'Lehner Inc', 4, '2023-11-30 08:46:19', 0, '2023-10-31 03:16:19', '2023-10-31 03:16:19', NULL),
(5, 'Gottlieb LLC', 5, '2023-11-30 08:46:19', 0, '2023-10-31 03:16:19', '2023-10-31 03:16:19', NULL),
(6, 'Rodriguez Ltd', 6, '2023-11-30 08:46:19', 0, '2023-10-31 03:16:19', '2023-10-31 03:16:19', NULL),
(7, 'Rowe-Casper', 7, '2023-11-30 08:46:19', 0, '2023-10-31 03:16:19', '2023-10-31 03:16:19', NULL),
(8, 'Daugherty, Feeney and Hartmann', 8, '2023-11-30 08:46:19', 0, '2023-10-31 03:16:19', '2023-10-31 03:16:19', NULL),
(9, 'Hoeger, Kozey and Ziemann', 9, NULL, 1, '2023-10-31 03:16:19', '2023-10-31 03:16:19', NULL),
(10, 'Hammes, Nicolas and Maggio', 10, NULL, 1, '2023-10-31 03:16:19', '2023-10-31 03:16:19', NULL),
(11, 'Praveem LTD', 11, '2023-11-30 09:58:12', 0, '2023-10-31 03:42:58', '2023-10-31 03:42:58', NULL),
(12, 'Praveem LTD', 11, '2023-11-30 09:14:13', 0, '2023-10-31 03:43:14', '2023-10-31 03:43:14', NULL),
(13, 'ClubWise', 11, '2023-11-30 01:43:02', 0, '2023-10-31 07:32:43', '2023-10-31 07:32:43', NULL),
(14, 'ClubWise', 11, '2023-11-30 01:02:03', 0, '2023-10-31 07:33:02', '2023-10-31 07:33:02', NULL),
(15, 'ClubWise', 11, '2023-11-30 05:56:54', 0, '2023-10-31 12:24:56', '2023-10-31 12:24:56', NULL),
(16, 'ClubWise', 11, '2023-11-30 06:51:30', 0, '2023-10-31 13:00:51', '2023-10-31 13:00:51', NULL),
(17, 'ClubWise', 11, '2023-11-30 06:31:31', 0, '2023-10-31 13:01:31', '2023-10-31 13:01:31', NULL),
(18, 'ClubWise', 11, '2023-11-30 06:55:32', 0, '2023-10-31 13:02:55', '2023-10-31 13:02:55', NULL),
(19, 'Aquil', 11, '2023-12-01 05:33:56', 0, '2023-11-01 00:26:33', '2023-11-01 00:26:33', NULL),
(20, 'Aquil', 11, '2023-12-01 10:39:00', 0, '2023-11-01 04:30:39', '2023-11-01 04:30:39', NULL),
(21, 'Aquil', 11, '2023-12-01 10:55:00', 0, '2023-11-01 04:30:55', '2023-11-01 04:30:55', NULL),
(22, 'Aquil', 11, '2023-12-01 10:48:02', 0, '2023-11-01 04:32:48', '2023-11-01 04:32:48', NULL),
(23, 'Aquil', 11, '2023-12-01 10:01:03', 0, '2023-11-01 04:33:01', '2023-11-01 04:33:01', NULL),
(24, 'Aquil', 11, '2023-12-01 10:40:03', 0, '2023-11-01 04:33:40', '2023-11-01 04:33:40', NULL),
(25, 'Demo Organisation', 11, '2023-12-01 12:38:50', 0, '2023-11-01 07:20:38', '2023-11-01 07:20:38', NULL),
(26, 'Test Org', 11, '2023-12-01 12:55:50', 0, '2023-11-01 07:20:55', '2023-11-01 07:20:55', NULL),
(27, 'Test Org', 11, '2023-12-01 01:35:10', 0, '2023-11-01 07:40:35', '2023-11-01 07:40:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Mrs. Libbie Wiegand DDS', 'addison98@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-10-31 03:16:19', '2023-10-31 03:16:19'),
(2, 'Hester Armstrong PhD', 'eino.fadel@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-10-31 03:16:19', '2023-10-31 03:16:19'),
(3, 'Caden Upton', 'lwiegand@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-10-31 03:16:19', '2023-10-31 03:16:19'),
(4, 'Prof. Perry Larson II', 'zohara@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-10-31 03:16:19', '2023-10-31 03:16:19'),
(5, 'Elyse Armstrong', 'damore.orval@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-10-31 03:16:19', '2023-10-31 03:16:19'),
(6, 'Cierra Haley Jr.', 'corbin.willms@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-10-31 03:16:19', '2023-10-31 03:16:19'),
(7, 'Reid Bechtelar I', 'alysa.wehner@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-10-31 03:16:19', '2023-10-31 03:16:19'),
(8, 'Katharina Aufderhar', 'marc63@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-10-31 03:16:19', '2023-10-31 03:16:19'),
(9, 'Prof. Lonny Heaney', 'xjohns@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-10-31 03:16:19', '2023-10-31 03:16:19'),
(10, 'Dr. Junior Rosenbaum IV', 'haley.tobin@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2023-10-31 03:16:19', '2023-10-31 03:16:19'),
(11, 'Test Account', 'test@test.com', '$2y$10$HJIeCK2aKqE3tHzYyvl7L.VDpUhia7UrE3NfOYN/FfS91fsHdov7G', '2023-10-31 03:22:57', '2023-10-31 03:22:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `organisations`
--
ALTER TABLE `organisations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organisations`
--
ALTER TABLE `organisations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
