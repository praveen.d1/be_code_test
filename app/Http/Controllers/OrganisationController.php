<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Organisation;
use App\Services\OrganisationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Mail;
use App\Transformers\OrganisationTransformer;
use App\Transformers\UserTransformer;

/**
 * Class OrganisationController
 * @package App\Http\Controllers
 */
class OrganisationController extends ApiController
{
    /**
     * @param OrganisationService $service
     *
     * @return JsonResponse
     */
    public function store(OrganisationService $service,Request $request): JsonResponse
    {
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        /** @var Organisation $organisation */
        $organisation = $service->createOrganisation($validatedData);
        $userRole = auth()->user();
        // Send confirmation email
        $userEmail = auth()->user()->email;
        Mail::send('emails.organisation-create', compact('organisation','userRole'), function($message) use($userEmail){
            $message->to($userEmail);
            $message->subject('Organisation Created');
        });
        return $this->transformItem('organisation', $organisation, ['user'])->respond();
    }

    public function listAll(OrganisationService $service,Request $request)
    {
        //Method 1 

        // $filter = $request->filter;
        // $Organisations = DB::table('organisations')->get();
        // $Organisation_Array = array();
        // foreach ($Organisations as $x) {
        //     if (isset($filter)) {
        //         if ($filter == 'subbed') {                        
        //             if ($x->subscribed == 1) {
        //                 array_push($Organisation_Array, $x);
        //             }
        //         } else if ($filter == 'trial') {
        //             if ($x->subscribed == 0) {
        //                 array_push($Organisation_Array, $x);
        //             }
        //         } else {
        //             array_push($Organisation_Array, $x);
        //         }
        //     } else {
        //         array_push($Organisation_Array, $x);
        //     }
        // }
        // return json_encode($Organisation_Array);

        //Method 2 Using transformer
        
        $filter = $request->input('filter', 'all');
        $organisations = $service->organisationList($filter);
        return $this->transformCollection('OrganisationList', $organisations, ['user'])->respond();
    }
}
