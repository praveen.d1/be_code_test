<?php

declare(strict_types=1);

namespace App\Services;

use App\Organisation;
use DB;

/**
 * Class OrganisationService
 * @package App\Services
 */
class OrganisationService
{
    /**
     * @param array $attributes
     *
     * @return Organisation
     */
    public function createOrganisation(array $attributes): Organisation
    {
        $organisation = new Organisation();
        $organisation->name = $attributes['name'];
        $date = now()->addDays(30);
        $organisation->owner_user_id = auth()->user()->id;
        $organisation->trial_end = $date->format('Y-m-d h:s:i');
        $organisation->save();
        return $organisation;
    }
    public function organisationList($filter)
    {
        $organisationData = Organisation::select('*');
        if ($filter === 'subbed') {
            $organisationData->where('subscribed', 1);
        } elseif ($filter === 'trial') {
            $organisationData->where('subscribed', 0);
        }
        $organisationData = $organisationData->get();
        return $organisationData;
    }
}
