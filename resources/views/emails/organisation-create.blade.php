<!DOCTYPE html>
<html>
<head>
    <title>Organisation Created</title>
</head>
<body>
    <p>
        Hello <b>{{$userRole->name}}</b>,
    </p>
    <p>
        Thank you for creating the organisation "<b>{{ $organisation->name }}</b>".
    </p>
    <p>
        Organisation Details :
    </p>
    <ul>
        <li>Organisation Name: {{ $organisation->name }}</li>
        <li>Owner User ID: {{ $organisation->owner_user_id }}</li>
        <li>Trial End Date: {{ $organisation->trial_end }}</li>
    </ul>
</body>
</html>